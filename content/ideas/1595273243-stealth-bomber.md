---
title: "Zoom Bombing for Introverts"
publishdate: 2020-07-20T15:27:00-0400
tags: ["ejm"]
---
Think Zoom bombing sounds fun but feeling camera shy? Now, introverts can "Zoom" under the radar, Black Ops style! Join our squadron of Stealth Zoom Bombers, and sneak in and out of calls before anyone realizes you were even there!

Compete with your fellow Stealth Bombers to see how many calls you can join before someone wonders who you are and why you won't speak! It will drive participants crazy trying to figure it out! What a thrill!

*Inspired by real people. Any resemblance to actual persons, living or dead, or actual events may not be purely coincidental.*
