---
title: Save the Environment. Solve Unemployment. Say, "You're welcome, world."
publishdate: 2020-07-31T15:14:00-0400
tags: ["ejm"]
---

Everyone is worried about the unemployment rate and watching what the coronavirus will continue to do to the economy if left unchecked.

At the same time, takeout has never been more popular. This generates a mass of waste that will go into a landfill if left unchecked.

Clearly, we can't leave either of them unchecked. It's time to check both. With our special Take-out recycle bins, you can throw all manner of styrofoam food containers in the bins. Then, our Takeout Advocates will collect and wash these containers by hand for re-use. We will re-sell the containers to restaurants at a discounted rate, and the world will be saved. At least for a little while longer.
