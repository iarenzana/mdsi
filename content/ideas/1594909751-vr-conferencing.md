---
title: "[Virtual] Reality Check."
publishdate: 2020-07-16T10:22:00-0400
tags: ["ejm"]
---
Problem: Conference calls are the new reality, so why is the tech so... old? 

Solution: Use people's memoji or bitmoji creations to populate a virtual conference room. A participant can then customize the base avatar of other attendees, and these changes will only be visible to that participant. For example, you could add a cowboy hat and handlebar mustache to your work rival and give a mohawk to your boss. Say goodbye to the days of fighting to stay awake, and hello to the days of fighting not to laugh! 

The location can also be selected by the participant. In addition to a high-rise conference room, other ready-made options include outer space, Pandora, Jurassic Park, on a yacht, in a submarine, and in a rooftop garden. In addition to these pre-installed options, users can customize other options from scratch. The sky isn't the limit; your imagination is. 
