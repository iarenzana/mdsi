---
title: Alpaca Husbandry
date: 2020-07-13
publishdate: 2020-07-13
tags: ["jmb"]
---
## Problem

The current gestation period for an Alpaca is around 1 year.


## Solution

Increase demand for Alpacas by opening a Alpaca Petting Zoo along with a Brewery and Pizzeria
