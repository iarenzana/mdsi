---
title: Tech travel, it's almost as good as time travel
publishdate: 2020-07-20T13:06:00-0400
tags: ["ejm"]
---
You know how you can't be two places at once?

Think again.

Allow me to introduce you to the child of COVID and deep fake technology. Now, you can be at your grandma's Zoom birthday party as well as that pesky committee meeting at the same time. All you have to do is upload a webcam video of you saying certain video chat phrases that will be inserted throughout the call. 

Prompts may include:
- "Did I lose you?"
- "Can anyone hear me?"
- "Are we waiting for anyone else?"
- "Can you repeat that, you froze up on me there for a minute"
- "Did someone just join?"
- "That is so true"
- "Sorry, I was on mute"

In addition to these recordings, we'll also request a few headshots taken from the webcam on your computer with you wearing a different shirt in each one. For an added fee, we'll even transcribe what others have said so that you can verify you haven't missed any crucial detail that may be followed up on. 
