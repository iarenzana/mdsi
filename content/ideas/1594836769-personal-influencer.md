---
title: Social Media Influencers, but without the social media
date: 2020-07-15
publishdate: 2020-07-15
tags: ["ejm"]
---
Forget what it's like to have a friend that's a bad influence? That talks you into doing something you'll later regret? Or even something you'll regret NOT doing? Forget what it's like to just... HAVE a friend? Maybe you had friends before COVID, or the internet, but with all the individualism of society today, you forget what it feels like to be old-school influenced, like your parents once were.

Now, you can get all the influence without any of the inevitable drama (unless you custom-order a side of "Drama" also). Former High School Debate team champs, now you can tell US how you want to be influenced. Just tell us which side to take and we'll be there whispering in your ear. For an all-immersive influencer experience, hire two of us to take on opposing sides. For a nominal extra fee, we may even throw in some punches (included in the "Drama" package). Alternatively, you can hire us to introduce to your family so they can be convinced that you do, indeed, have friends. Then, they can once again blame all your faults on us, shifting attention away from the consequences of your choices.

Some call us "Persuasive Consultants." Others call us "Personal Influencers." Still others call us "The worst friends-for-hire ever." Granted, that was the guy we talked into getting a face tattoo. So really, we are the best bad-influencers out there.
