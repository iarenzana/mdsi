---
title: World's Largest Trip Planner
date: 2020-08-111
publishdate: 2020-08-11
tags: ["jmb"]
---
What were once novelty roadside attractions before the pandemic of 2020 are now America’s hottest travel destinations. From the “World’s Largest Ball of Twine” to the “World’s Largest Lobster” our nation's highways are filled with endless amusement for the whole family!

However as these attractions were designed as shorts stops there’s not much to do once the awe of the attraction wears off.   That is where The World’s Largest Trip Planner comes to your rescue.  Simply enter your home address and your destination i.e “The World’s Largest Fire Hydrant.”  Our patented AI technology will then plan for you the route, lodging and entertainment stops to complete your “World’s Largest Experience”  

