---
title: "Testifly to your destination!"
publishdate: 2020-09-30T09:15:28-0400
tags: ["ejm"]
---

Does your travel destination require a negative COVID test within 72 hours of your arrival? If you're like us, you've noticed that tests are hard to come by, and prompt results are even harder. 

Now, with Testifly, we'll take care of both of these for you! Get tested upon arrival at your departure airport and Have Your Results in Hand by the Time You Land!™️
