---
title: Panamerican Ferry Services
date: 2020-07-15
publishdate: 2020-07-15
tags: ["iar"]
---
The Problem: Can't travel from Eurasia to Argentina.

The Solution: Two services: Bering straight ice-breaker ferries and Darien gap ferries.

The Execution: Multi-class (economy, and first class) vessels to ferry people (and vehicles) between Russia and Alaska and Panama and Colombia. Sell combo tickets for those doing the World Roadtrip. Advertise as the company that made the World Roadtrip possible at last.
