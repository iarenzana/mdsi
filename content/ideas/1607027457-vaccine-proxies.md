---
title: "Vaccine Proxies"
publishdate: 2020-12-02T15:33:28-0400
tags: ["jmb"]
---

Stressed about the new COVID-19 vaccines, but need one for travel or work? Why take the risk when someone else can do it for you?  

That’s where our team of no fear individuals come to your assistance.   We will pair you with a proxy willing to take the vaccine on your behalf. Once they have completed the vaccine we will facilitate all the proper paperwork to insure the world you’ve been vaccinated.  
P.S this service is not available for anti-vaxxers we wouldn’t want to contradict your beliefs.   

