---
title: Sink Different.
date: 2020-07-13
publishdate: 2020-07-13
tags: ["ejm"]
---
Feeling under pressure? Is your business taking a dive? Are your earnings sub-par? When everything else in your life is going under, you might as well go, too.

Social distance in style. Rent a submarine to escape the 2020 apocalypse in peace, surrounded by sea creatures and isolated from the rest of the infected world.
