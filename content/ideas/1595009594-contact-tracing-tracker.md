---
title: Pretend you're a spy. Make informed choices. Be the hero we don't deserve.
publishdate: 2020-07-17T14:50:00-0400
tags: ["ejm"]
---
Still have that one friend that refuses to acknowledge COVID is real? All you have to do is place a tracker on them, and it will do contact tracing for you. The technology is integrated with public transportation lines so it will know if your friend used these services. And, since you know they aren't wearing a mask, you can monitor their movements on your smart device to determine whether or not it would be wise to meet up with them. 

To help you as an objective third party [app], we will rank the risk levels of your traced friends using the following scale:

1 = Movements were indistinguishable from someone on house arrest. Low risk.

2 = Movements like someone in solitary confinement. They were alone, but the conditions were unsanitary. Visit them at your own risk.

3 = Basically someone on house arrest who figured out how to tamper with their ankle bracelet to escape the confines of their home a couple times. They still had to stay under the radar, so relatively low risk.

4 = Movements like someone in General Pop. Definitely exposed to COVID, stay away.

5 = PRISON BREAK! If you meet up with someone rated "5" you will be guilty of aiding and abetting the 'rona.
